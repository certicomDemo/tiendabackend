package pe.com.certicom.tienda.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.certicom.tienda.model.Cliente;
import pe.com.certicom.tienda.service.ClienteService;

@RestController
@RequestMapping ("/cliente/")
public class ClienteController {
	
	@Autowired
	private ClienteService clienteService;
	
	@GetMapping
	private ResponseEntity<List<Cliente>> getAllClientes () {
		
		return ResponseEntity.ok(clienteService.findAll());	
	}
	
	@GetMapping ("{id}")
	private ResponseEntity<Cliente> getClienteById(@PathVariable("id") Long idCliente) {
		return ResponseEntity.ok(clienteService.findClienteById(idCliente));
	}
	
	@PostMapping
	private ResponseEntity<Cliente> saveCliente(@RequestBody Cliente cliente){
		
		try {
			  Cliente clienteGuardado = clienteService.save(cliente);
			  if(clienteGuardado != null) {
		        return ResponseEntity.created(new URI("/cliente/"+clienteGuardado.getId())).body(clienteGuardado);
			  } else {
				  throw new Exception();
			  }
			
		}catch (Exception e) {
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();	
		}
	}
}
