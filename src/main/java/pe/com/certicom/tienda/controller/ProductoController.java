package pe.com.certicom.tienda.controller;

import java.net.URI;
import java.util.Collections;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.certicom.tienda.model.Producto;
import pe.com.certicom.tienda.service.ProductoService;

@RestController
@RequestMapping ("/producto/")
public class ProductoController {
	
	
	@Autowired
	private ProductoService productoService;
	
	@GetMapping
	private ResponseEntity<List<Producto>> getAllProductos () {
		
		return ResponseEntity.ok(productoService.findAll());	
	}
	
	@GetMapping ("{id}")
	private ResponseEntity<Producto> getProductoById(@PathVariable("id") Long idProducto) {
		
		return ResponseEntity.ok(productoService.findProductoById(idProducto));
	}
	
	@PostMapping
	private ResponseEntity<Producto> saveProducto(@RequestBody Producto producto) {
		
		try {
			 Producto productoGuardado = productoService.save(producto);
			  if(productoGuardado != null) {
		        return ResponseEntity.created(new URI("/producto/"+productoGuardado.getId())).body(productoGuardado);
			  } else {
				  throw new Exception();
			  }
			
		}catch (Exception e) {
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();	
		}
	}
	
	@GetMapping ("borrar/{id}")
	private Map<String, Boolean> deleteProductoById(@PathVariable("id") Long idProducto) {
		productoService.deleteById(idProducto);
		return Collections.singletonMap("success", true);
	}

}
