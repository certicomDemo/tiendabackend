package pe.com.certicom.tienda.controller;

import java.net.URI;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import pe.com.certicom.tienda.model.Venta;
import pe.com.certicom.tienda.service.VentaService;

@RestController
@RequestMapping ("/ventas")
@CrossOrigin(origins = "*")
public class VentaController {
	
	@Autowired
	private VentaService ventaService;
	
	@GetMapping
	private ResponseEntity<List<Venta>> getAllVentas () {
		
		return ResponseEntity.ok(ventaService.findAll());	
	}
	
	@GetMapping ("/{id}")
	private ResponseEntity<Venta> getVentaById(@PathVariable("id") Long idVenta) {
		return ResponseEntity.ok(ventaService.findVentaById(idVenta));
	}
	
	@GetMapping ("/cliente/{id}")
	private ResponseEntity<List<Venta>> getAllVentasByCliente(@PathVariable("id") Long idCliente) {
		return ResponseEntity.ok(ventaService.findAllByCliente(idCliente));
	}
	
	@GetMapping ("/fecha/{fecha}")
	private ResponseEntity<List<Venta>> getAllVentasByDate(@PathVariable("fecha") String fecha)  {
		
		return ResponseEntity.ok(ventaService.findAllByDate(fecha));
	}
	
	@PostMapping
	private ResponseEntity<Venta> saveVenta(@RequestBody Venta venta) {
		
		try {
			 Venta ventaGuardada = ventaService.save(venta);
			  if(ventaGuardada != null) {
		        return ResponseEntity.created(new URI("/ventas/"+ ventaGuardada.getId())).body(ventaGuardada);
			  } else {
				  throw new Exception();
			  }
			
		}catch (Exception e) {
			
			return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();	
		}
	}
	
	
}
