package pe.com.certicom.tienda;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.stereotype.Component;

import pe.com.certicom.tienda.model.Cliente;
import pe.com.certicom.tienda.model.Producto;
import pe.com.certicom.tienda.model.Venta;
import pe.com.certicom.tienda.model.VentaDetalle;
import pe.com.certicom.tienda.repository.ClienteRepository;
import pe.com.certicom.tienda.repository.ProductoRepository;
import pe.com.certicom.tienda.repository.VentaRepository;

@SpringBootApplication
public class TiendaBackendApplication {

	public static void main(String[] args) {
		SpringApplication.run(TiendaBackendApplication.class, args);
	}

}

@Component
class DemoCommandLineRunner implements CommandLineRunner{

	@Autowired
	private ClienteRepository clienteRepository;
	
	@Autowired
	private ProductoRepository productoRepository;
	
	@Autowired
	private VentaRepository ventaRepository;
	

	@Override
	public void run(String... args) throws Exception {

		Cliente cli1 = new Cliente("Jesus", "Marallano  Barbaran", "44178155", "944246546", "jesus.mb.1902@gmail.com");
		clienteRepository.save(cli1);
		
		Cliente cli2 = new Cliente("Miriam", "Sullcaray  Palomino", "44127162", "944246548", "miriam.sp.1902@gmail.com");
		clienteRepository.save(cli2);
		
		Producto prod1 = new Producto("Arroz", 19.90F);
		productoRepository.save(prod1);
		
		Producto prod2 = new Producto("Azucar", 11.90F);
		productoRepository.save(prod2);
		
		Producto prod3 = new Producto("Fideo", 4.90F);
		productoRepository.save(prod3);
			
		Producto prod4 = new Producto("Leche", 6.90F);
		productoRepository.save(prod4);
		
		SimpleDateFormat formatter1=new SimpleDateFormat("yyyy-MM-dd");
		String sDate1="2021-08-30";  
		Date date1=formatter1.parse(sDate1);
		
		/////////////////////////////////////////////////////////////////
		
		VentaDetalle vd1 = new VentaDetalle(prod1, 1);
		VentaDetalle vd2 = new VentaDetalle(prod2, 3);
		
		List<VentaDetalle> listaDetalleVenta = new ArrayList<>();
		listaDetalleVenta.add(vd1);
		listaDetalleVenta.add(vd2);
		
		Venta venta1 = new Venta(cli1, date1);
		venta1.setVentaDetalle(listaDetalleVenta);
		ventaRepository.save(venta1);
		
		System.out.println("Total a pagar: S/."+ venta1.getTotal());
		
		/////////////////////////////////////////////////////////////////////
		
		String sDate2="2021-08-31";  
		Date date2=formatter1.parse(sDate2);
		
		VentaDetalle vd3 = new VentaDetalle(prod3, 2);
		VentaDetalle vd4 = new VentaDetalle(prod4, 4);
		
		listaDetalleVenta = new ArrayList<>();
		listaDetalleVenta.add(vd3);
		listaDetalleVenta.add(vd4);
		
		Venta venta2 = new Venta(cli2, date2);
		venta2.setVentaDetalle(listaDetalleVenta);
		ventaRepository.save(venta2);
		
		System.out.println("Total a pagar: S/."+ venta2.getTotal());

	}
}
