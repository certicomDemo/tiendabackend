package pe.com.certicom.tienda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.certicom.tienda.model.Cliente;

public interface ClienteRepository extends JpaRepository<Cliente, Long> {


}
