package pe.com.certicom.tienda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.certicom.tienda.model.Venta;

public interface VentaRepository extends JpaRepository<Venta, Long>{
	
}
