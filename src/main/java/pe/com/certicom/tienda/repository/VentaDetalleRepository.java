package pe.com.certicom.tienda.repository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.certicom.tienda.model.VentaDetalle;

public interface VentaDetalleRepository extends JpaRepository<VentaDetalle, Long>{ 
	
}
