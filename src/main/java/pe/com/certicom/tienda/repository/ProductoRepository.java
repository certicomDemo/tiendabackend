package pe.com.certicom.tienda.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import pe.com.certicom.tienda.model.Producto;

public interface ProductoRepository extends JpaRepository<Producto, Long> {

}
