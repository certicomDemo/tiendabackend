package pe.com.certicom.tienda.service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import pe.com.certicom.tienda.model.Venta;
import pe.com.certicom.tienda.repository.VentaRepository;

@Service
public class VentaService implements VentaRepository{
	
	@Autowired
	private VentaRepository ventaRepository;

	@Override
	public List<Venta> findAll() {
		
		return ventaRepository.findAll();
	}
    
	public List<Venta> findAllByCliente(Long idCliente) {
		
		List<Venta> ventasRespuesta = new ArrayList<>();
		
		List<Venta> ventas = ventaRepository.findAll();
		
		for(Venta venta: ventas){
			
			if(venta.getCliente().getId()== idCliente) {
				ventasRespuesta.add(venta);
			}
		}
		
		return ventasRespuesta;
	}
	
	public Venta findVentaById(Long idVenta) {
		
		List<Venta> ventas = ventaRepository.findAll();
		
		for(Venta venta: ventas){
			
			if(venta.getId()== idVenta) {
				return venta;
			}
		}
		
	     return null;
	}
	
    public List<Venta> findAllByDate(String fecha) {
		
		List<Venta> ventasRespuesta = new ArrayList<>();
		
		List<Venta> ventas = ventaRepository.findAll();
		
		String pattern = "yyyy-MM-dd";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

		for(Venta venta: ventas){
			
			String dateDB = simpleDateFormat.format(venta.getFecha());
			
			if(dateDB.equals(fecha)) {
				ventasRespuesta.add(venta);
			}
		}
		
		return ventasRespuesta;
	}

	@Override
	public List<Venta> findAll(Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Venta> findAllById(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Venta> List<S> saveAll(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void flush() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Venta> S saveAndFlush(S entity) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Venta> List<S> saveAllAndFlush(Iterable<S> entities) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteAllInBatch(Iterable<Venta> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllByIdInBatch(Iterable<Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAllInBatch() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Venta getOne(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Venta getById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Venta> List<S> findAll(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Venta> List<S> findAll(Example<S> example, Sort sort) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Page<Venta> findAll(Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Venta> S save(S entity) {
		// TODO Auto-generated method stub
		return ventaRepository.save(entity);
	}

	@Override
	public Optional<Venta> findById(Long id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean existsById(Long id) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public long count() {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		ventaRepository.deleteById(id);
	}

	@Override
	public void delete(Venta entity) {
		// TODO Auto-generated method stub
		ventaRepository.delete(entity);
	}

	@Override
	public void deleteAllById(Iterable<? extends Long> ids) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll(Iterable<? extends Venta> entities) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteAll() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <S extends Venta> Optional<S> findOne(Example<S> example) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Venta> Page<S> findAll(Example<S> example, Pageable pageable) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <S extends Venta> long count(Example<S> example) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public <S extends Venta> boolean exists(Example<S> example) {
		// TODO Auto-generated method stub
		return false;
	}

}
